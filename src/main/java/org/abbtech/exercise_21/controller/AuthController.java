package org.abbtech.exercise_21.controller;

import jakarta.validation.Valid;
import org.abbtech.exercise_21.dto.request.LoginRequestDTO;
import org.abbtech.exercise_21.dto.request.UserDTO;
import org.abbtech.exercise_21.dto.response.JWTResponseDTO;
import org.abbtech.exercise_21.dto.response.UserRegisterResponseDTO;
import org.abbtech.exercise_21.service.JWTService;
import org.abbtech.exercise_21.service.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/auth")
@Valid
public class AuthController {

    private final UserService userService;

    private final JWTService jwtService;

    private final AuthenticationManager authenticationManager;

    public AuthController(UserService userService, JWTService jwtService, AuthenticationManager authenticationManager) {
        this.userService = userService;
        this.jwtService = jwtService;
        this.authenticationManager = authenticationManager;
    }

    @GetMapping("/login")
    public ResponseEntity<String> test() {
        return ResponseEntity.ok("Hello World");
    }

    @PostMapping("/login")
    public ResponseEntity<JWTResponseDTO> loginUser(@Valid @RequestBody LoginRequestDTO loginRequestDTO) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequestDTO.getUsername(), loginRequestDTO.getPassword()));
        if (authentication.isAuthenticated()) {
            return ResponseEntity.ok().body(JWTResponseDTO.builder().accessToken(jwtService.generateToken(loginRequestDTO.getUsername())).build());
        } else {
            throw new UsernameNotFoundException("These credentials not found our system");
        }
    }

    @PostMapping("/register")
    public ResponseEntity<UserRegisterResponseDTO> registerUser(@Valid  @RequestBody UserDTO userDto) {
        return ResponseEntity.ok(userService.addUser(userDto));
    }
}
