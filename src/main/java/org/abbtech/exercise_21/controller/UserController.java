package org.abbtech.exercise_21.controller;

import jakarta.validation.Valid;
import org.abbtech.exercise_21.dto.request.UserDTO;
import org.abbtech.exercise_21.dto.response.CustomResponseDTO;
import org.abbtech.exercise_21.dto.response.UserListResponseDTO;
import org.abbtech.exercise_21.service.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("/users")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping
    public ResponseEntity<CustomResponseDTO<List<UserListResponseDTO>>> createUser(@Valid @RequestBody UserDTO userDTO){
        userService.addUser(userDTO);
        return ResponseEntity.ok(new CustomResponseDTO<>(LocalDateTime.now(),200,"User Created Successfully!"));
    }
    @GetMapping
    public ResponseEntity<CustomResponseDTO<UserListResponseDTO>> getAllUsers(){
        return ResponseEntity.ok(new CustomResponseDTO<>(LocalDateTime.now(), 200, "User Created Successfully!", userService.getAllUsers()));
    }

}
