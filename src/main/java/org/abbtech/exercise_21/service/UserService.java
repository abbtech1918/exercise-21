package org.abbtech.exercise_21.service;

import jakarta.transaction.Transactional;
import org.abbtech.exercise_21.dto.request.PostDTO;
import org.abbtech.exercise_21.dto.request.UserDTO;
import org.abbtech.exercise_21.dto.response.UserListResponseDTO;
import org.abbtech.exercise_21.dto.response.UserRegisterResponseDTO;
import org.abbtech.exercise_21.exception.errors.UserNotFoundException;
import org.abbtech.exercise_21.mapper.UserMapper;
import org.abbtech.exercise_21.model.Comment;
import org.abbtech.exercise_21.model.Post;
import org.abbtech.exercise_21.model.Role;
import org.abbtech.exercise_21.model.User;
import org.abbtech.exercise_21.repository.RoleRepository;
import org.abbtech.exercise_21.repository.UserRepository;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class UserService {

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;

    private final PasswordEncoder passwordEncoder;
    private final UserMapper userMapper;

    public UserService(UserRepository userRepository, RoleRepository roleRepository, PasswordEncoder passwordEncoder,
                       UserMapper userMapper) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.passwordEncoder = passwordEncoder;
        this.userMapper = userMapper;
    }

    @Transactional
    public UserRegisterResponseDTO addUser(UserDTO userDTO) {
        Role userRole = roleRepository.findByRoleName("USER");
        if (userRole == null) {
            userRole = new Role("USER", "Default role for registered users");
            roleRepository.save(userRole);
        }
        User user = new User(userDTO.name(),userDTO.username(), passwordEncoder.encode(userDTO.password()));
        user.setRoles(new HashSet<>(List.of(userRole)));
        userRepository.save(user);
        UserRegisterResponseDTO userRegisterResponseDTO = new UserRegisterResponseDTO(user.getId(), user.getUsername());
        userRegisterResponseDTO.setRole("USER");
        return userRegisterResponseDTO;
    }

    public List<UserListResponseDTO> getAllUsers(){
        return userRepository.findAll().stream().map(userMapper::modelToDto).collect(Collectors.toList());
    }

}
