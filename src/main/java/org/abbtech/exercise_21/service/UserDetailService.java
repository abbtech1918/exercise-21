package org.abbtech.exercise_21.service;

import lombok.RequiredArgsConstructor;
import org.abbtech.exercise_21.dto.response.CustomUserDetailsDTO;
import org.abbtech.exercise_21.model.Role;
import org.abbtech.exercise_21.model.User;
import org.abbtech.exercise_21.repository.UserRepository;
import org.springframework.http.HttpStatusCode;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class UserDetailService implements UserDetailsService {

    private final UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User userInfo = userRepository.findUserByUsernameIgnoreCase(username).orElseThrow(() -> new ResponseStatusException(HttpStatusCode.valueOf(403)));

        Set<GrantedAuthority> authoritySet = new HashSet<>();
        for (Role userRole : userInfo.getRoles()) {
            authoritySet.add(new SimpleGrantedAuthority(userRole.getRoleName().toUpperCase()));
        }

        return org.springframework.security.core.userdetails.User.builder().username(userInfo.getUsername())
                .password(userInfo.getPassword())
                .authorities(authoritySet).build();
    }
}
