package org.abbtech.exercise_21.dto.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

@JsonPropertyOrder({"timestamp", "status", "message", "data"})
@Getter
@Setter
@Builder
public class CustomResponseDTO<T> {
    @JsonProperty("timestamp")
    @JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss")
    LocalDateTime timeStamp;
    Integer status;
    String message;
    List<T> data;

    public CustomResponseDTO(LocalDateTime timeStamp, Integer status, String message) {
        this.timeStamp = timeStamp;
        this.status = status;
        this.message = message;
    }

    public CustomResponseDTO(LocalDateTime timeStamp, Integer status, String message, List<T> data) {
        this.timeStamp = timeStamp;
        this.status = status;
        this.message = message;
        this.data = data;
    }
}


