package org.abbtech.exercise_21.dto.response;

import lombok.*;
import org.abbtech.exercise_21.model.Role;

import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserListResponseDTO {
    private Long id;
    private String name;
    private String username;
    private Set<Role> roles;
}
