package org.abbtech.exercise_21.dto.response;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class JWTResponseDTO {
    private String accessToken;
}
