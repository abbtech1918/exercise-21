package org.abbtech.exercise_21.dto.response;

import java.util.Date;

public record ErrorDetailDTO(String path, String errorMessage, String errorCode, Integer status, Date timeStamp) { }
