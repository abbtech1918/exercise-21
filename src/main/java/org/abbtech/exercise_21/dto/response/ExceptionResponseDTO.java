package org.abbtech.exercise_21.dto.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Getter;
import lombok.Setter;

import java.util.Map;

@JsonPropertyOrder({"timestamp", "status", "error", "path", "validation_errors"})
@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Setter
public class ExceptionResponseDTO {
    @JsonProperty("timestamp")
    String timeStamp;
    Integer status;
    String error;
    String path;

    @JsonProperty("validation_errors")
    Map<String, String> validationErrors;
    public ExceptionResponseDTO(String timeStamp, Integer status, String error, String path) {
        this.timeStamp = timeStamp;
        this.status = status;
        this.error = error;
        this.path = path;
    }
}


