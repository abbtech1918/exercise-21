package org.abbtech.exercise_21.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserRegisterResponseDTO {
    private Long id;
    private String username;
    private String role;

    public UserRegisterResponseDTO(String username) {
        this.username = username;
    }

    public UserRegisterResponseDTO(Long id, String username) {
        this.id = id;
        this.username = username;
    }
}
