package org.abbtech.exercise_21.dto.request;

import jakarta.validation.Valid;
import jakarta.validation.constraints.Size;

import java.util.List;

public record PostDTO(
        @Size(min = 3, max = 20) String title,
        @Size(min = 30, max = 150) String content,
        @Valid @Size(min = 2, max = 5) List<CommentDTO> commentDtoList) { }
