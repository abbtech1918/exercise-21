package org.abbtech.exercise_21.dto.request;

import jakarta.validation.constraints.*;

public record UserDTO(
        @NotNull @NotBlank @Size(min = 4, max = 15) String name,
        @NotNull @NotBlank @Size(min = 4, max = 15) String username,
        @Size(min = 4, max = 15) @Pattern(regexp = "^(?=.*[A-Z].*[A-Z])(?=.*[!@#$&*])(?=.*[0-9].*[0-9])(?=.*[a-z].*[a-z].*[a-z]).*$",
                message = "not a valid password") String password) {
}