package org.abbtech.exercise_21.dto.request;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;

public record UserAdminDTO(
        @NotNull @NotBlank @Size(min = 4, max = 15) String name,
        @NotNull @NotBlank @Size(min = 4, max = 15) String username,
        @NotNull @NotBlank @Size(min = 4, max = 15) Long role_id,
        @Size(min = 4, max = 15) @Pattern(regexp = "^(?=.*[A-Z].*[A-Z])(?=.*[!@#$&*])(?=.*[0-9].*[0-9])(?=.*[a-z].*[a-z].*[a-z]).*$",
                message = "not a valid password") String password) {
}