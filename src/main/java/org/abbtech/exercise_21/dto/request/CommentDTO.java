package org.abbtech.exercise_21.dto.request;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

public record CommentDTO(@NotNull @NotBlank @Size(min = 5, max = 100) String content) {
}
