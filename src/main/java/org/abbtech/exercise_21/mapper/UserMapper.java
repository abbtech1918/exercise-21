package org.abbtech.exercise_21.mapper;

import org.abbtech.exercise_21.dto.response.UserListResponseDTO;
import org.abbtech.exercise_21.model.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface UserMapper {
    UserListResponseDTO modelToDto(User user);
    User dtoToModel(UserListResponseDTO userListResponseDTO);
}
