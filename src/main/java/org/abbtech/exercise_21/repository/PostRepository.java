package org.abbtech.exercise_21.repository;

import org.abbtech.exercise_21.model.Post;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface PostRepository extends JpaRepository<Post, Integer> {
//    @Query("SELECT DISTINCT p FROM Post p " +
//            "LEFT JOIN FETCH p.comments " +
//            "WHERE p.id = :postId")
//    List<Post> findPostsWithCommentsByPostId(@Param("postId") Integer postId);
}
