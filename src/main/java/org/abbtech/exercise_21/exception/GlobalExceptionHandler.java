package org.abbtech.exercise_21.exception;

import jakarta.servlet.http.HttpServletRequest;
import org.abbtech.exercise_21.dto.response.ErrorDetailDTO;
import org.abbtech.exercise_21.dto.response.ExceptionResponseDTO;
import org.abbtech.exercise_21.exception.errors.AuthRequestException;
import org.abbtech.exercise_21.exception.errors.BadRequestException;
import org.abbtech.exercise_21.exception.errors.GeneralTechException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(BadRequestException.class)
    public ResponseEntity<ErrorDetailDTO> handleBadRequestException(BadRequestException exception,
                                                                    WebRequest webRequest) {
        return ResponseEntity
                .status(exception.getBadRequestExceptionEnum().getStatusCode())
                .body(new ErrorDetailDTO(webRequest.getContextPath(),
                        exception.getBadRequestExceptionEnum().toString(),
                        exception.getBadRequestExceptionEnum().getErrorCode(),
                        exception.getBadRequestExceptionEnum().getStatusCode()
                        , new Date()));
    }

    @ExceptionHandler(GeneralTechException.class)
    public ResponseEntity<ErrorDetailDTO> handleGeneralTechException(GeneralTechException exception,
                                                                     WebRequest webRequest) {
        return ResponseEntity
                .status(exception.getGeneralExceptionEnum().getStatusCode())
                .body(new ErrorDetailDTO(webRequest.getContextPath(),
                        exception.getGeneralExceptionEnum().toString(),
                        exception.getGeneralExceptionEnum().getErrorCode(),
                        exception.getGeneralExceptionEnum().getStatusCode()
                        , new Date()));
    }

    @ExceptionHandler(AuthRequestException.class)
    public ResponseEntity<ErrorDetailDTO> handleAuthRequestException(AuthRequestException authRequestException) {
        return ResponseEntity
                .status(authRequestException.getAuthRequestExceptionEnum().getStatusCode())
                .body(new ErrorDetailDTO("*",
                        authRequestException.getClass().toString(),
                        authRequestException.getAuthRequestExceptionEnum().getErrorCode(),
                        authRequestException.getAuthRequestExceptionEnum().getStatusCode()
                        , new Date()));
    }

    @ExceptionHandler(value = {MethodArgumentNotValidException.class})
    public ResponseEntity<ExceptionResponseDTO> handleMethodArgumentNotValidException(
            MethodArgumentNotValidException exception, HttpServletRequest request) {
        BindingResult result = exception.getBindingResult();
        Map<String, String> validationErrors = new HashMap<>();
        result.getFieldErrors()
                .forEach(validationError -> validationErrors.put(validationError.getField(),
                        validationError.getDefaultMessage()));

        ExceptionResponseDTO exceptionResponse = new ExceptionResponseDTO(LocalDateTime.now().toString(), 400,
                "Invalid request", request.getServletPath());
        exceptionResponse.setValidationErrors(validationErrors);
        return new ResponseEntity<>(exceptionResponse, HttpStatus.UNPROCESSABLE_ENTITY);
    }
}
