package org.abbtech.exercise_21.exception.enums;

import lombok.Getter;

@Getter
public enum AuthRequestExceptionEnum {
    UNAUTHORIZED("AUTH-BIZ-0001", 401),
    FORBIDDEN("AUTH-BIZ-0002", 403);

    private final String errorCode;
    private final int statusCode;

    AuthRequestExceptionEnum(String errorCode, int statusCode) {
        this.errorCode = errorCode;
        this.statusCode = statusCode;
    }
}
