package org.abbtech.exercise_21.exception.errors;

import lombok.Getter;
import org.abbtech.exercise_21.exception.enums.BadRequestExceptionEnum;

@Getter
public class BadRequestException extends RuntimeException {
    private final BadRequestExceptionEnum badRequestExceptionEnum;

    public BadRequestException(BadRequestExceptionEnum badRequestExceptionEnum) {
        super(badRequestExceptionEnum.toString());
        this.badRequestExceptionEnum = badRequestExceptionEnum;
    }

    public BadRequestException(BadRequestExceptionEnum badRequestExceptionEnum, Throwable throwable) {
        super(badRequestExceptionEnum.toString(), throwable);
        this.badRequestExceptionEnum = badRequestExceptionEnum;
    }
}
