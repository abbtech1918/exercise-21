package org.abbtech.exercise_21.exception.errors;

import lombok.Getter;
import org.abbtech.exercise_21.exception.enums.GeneralExceptionEnum;

@Getter
public class GeneralTechException extends RuntimeException {
    private final GeneralExceptionEnum generalExceptionEnum;

    public GeneralTechException(GeneralExceptionEnum generalExceptionEnum) {
        this.generalExceptionEnum = generalExceptionEnum;
    }

    public GeneralTechException(GeneralExceptionEnum generalExceptionEnum, Throwable throwable) {
        super(generalExceptionEnum.toString(), throwable);
        this.generalExceptionEnum = generalExceptionEnum;
    }
}
