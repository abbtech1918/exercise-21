package org.abbtech.exercise_21.exception.errors;

import com.google.gson.Gson;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.abbtech.exercise_21.dto.response.ErrorDetailDTO;
import org.abbtech.exercise_21.exception.enums.AuthRequestExceptionEnum;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

@Component
public class CustomAuthenticationEntryPoint implements AuthenticationEntryPoint {


    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException, ServletException {
        response.setContentType("application/json");
        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        Gson gson = new Gson();
        ErrorDetailDTO errorDetail = new ErrorDetailDTO(
                request.getContextPath(),
                AuthRequestExceptionEnum.class.getName(),
                AuthRequestExceptionEnum.UNAUTHORIZED.getErrorCode(),
                AuthRequestExceptionEnum.UNAUTHORIZED.getStatusCode(),
                new Date());
        String errorResponse = gson.toJson(errorDetail);
        try (PrintWriter writer = response.getWriter()) {
            writer.write(errorResponse);
            writer.flush();
        }
    }
}
