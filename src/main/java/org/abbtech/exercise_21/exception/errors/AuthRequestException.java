package org.abbtech.exercise_21.exception.errors;

import lombok.Getter;
import org.abbtech.exercise_21.exception.enums.AuthRequestExceptionEnum;

@Getter
public class AuthRequestException extends RuntimeException {
    private final AuthRequestExceptionEnum authRequestExceptionEnum;

    public AuthRequestException(AuthRequestExceptionEnum authRequestExceptionEnum) {
        super(authRequestExceptionEnum.toString());
        this.authRequestExceptionEnum = authRequestExceptionEnum;
    }

    public AuthRequestException(AuthRequestExceptionEnum authRequestExceptionEnum, Throwable throwable) {
        super(authRequestExceptionEnum.toString(), throwable);
        this.authRequestExceptionEnum = authRequestExceptionEnum;
    }
}
